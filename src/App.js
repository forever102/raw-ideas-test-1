import React, { Component } from 'react';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';

import { Nav, NavItem } from 'react-bootstrap';

import './App.css';
import TableTab from './components/TableTab';
import ModalTab from './components/ModalTab';
import VideoTab from './components/VideoTab';

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedTabIndex: null
    };
  }

  componentDidMount() { }

  handleSelect(eventKey, event) {
    this.setState({ selectedTabIndex: eventKey });
  }

  render() {
    return (
      <BrowserRouter>
        <div className="container">
          <Nav bsStyle="tabs" activeKey={this.state.selectedTabIndex ? this.state.selectedTabIndex : '1'} onSelect={k => this.handleSelect(k)}>
            <NavItem eventKey="1" componentClass={Link} href="/" to="/">Table Tab</NavItem>
            <NavItem eventKey="2" componentClass={Link} href="/ModalTab" to="/ModalTab">Modal Tab</NavItem>
            <NavItem eventKey="3" componentClass={Link} href="/VideoTab" to="/VideoTab">Video Tab</NavItem>
          </Nav>
          
          <Switch>
            <Route exact path='/' component={TableTab} />
            <Route path='/ModalTab' component={ModalTab} />
            <Route path='/VideoTab' component={VideoTab} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
