import React from 'react';

import Modal from 'react-bootstrap/lib/Modal';

export default class SampleModal extends React.Component {
    constructor(props) {
        super(props);

        // States.
        this.state = { show: false };

        // Variables.
        this.sampleModalRef = React.createRef();

        // Shared stuffs.
    }

    //#region Helper methods.
    //#endregion.

    //#region Component Life Cycle.
    componentDidMount() { }
    //#endregion.

    //#region DOM events.
    show() {
        this.setState({ show: true }, () => { });
    }

    clickYes(e) {
        this.setState({ show: false }, () => { });
    }
    //#endregion.

    render() {
        return (
            <div>
                <Modal ref={this.sampleModalRef} show={this.state.show} onHide={(e) => this.setState({ show: false })}>
                    <Modal.Header>
                        <Modal.Title>Sample Modal</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        Hello there! I am the Modal created by React-Bootstrap. But you can use Material-UI to create me.
                    </Modal.Body>

                    <Modal.Footer>
                        <button type="button" className="btn btn-default" onClick={(e) => this.setState({ show: false })}>No</button>
                        <button type="button" className="btn btn-primary" onClick={(e) => this.clickYes(e)}>Yes</button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}