import React from 'react';

export default class VideoTab extends React.Component {
    // constructor(props) {
    //     super(props);
    // }

    //#region Helper methods.
    //#endregion.

    //#region Component Life Cycle.
    //#endregion.

    //#region DOM events.
    //#endregion.

    render() {
        return (
            <div>
                <br />
                <div className="embed-responsive embed-responsive-16by9">
                    {/* <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/A6XUVjK9W4o" frameborder="0" allowfullscreen></iframe> */}
                    <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/6ZnfsJ6mM5c" frameBorder="0" allowFullScreen></iframe>
                </div>
            </div>
        )
    }
}