import React from 'react';

export default class TableTab extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            todos: []
        };
    }

    //#region Helper methods.
    getTodos() {
        let isOk = false;
        let response = null;
        let error = null;

        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(res => res.json())
            .then(data => {
                isOk = true;
                response = data;
            })
            .catch((err) => { error = err })
            .then(() => {
                if (isOk) {
                    if (response && response.length > 0) {
                        // console.info('response is: ' + JSON.stringify(response));
                        this.setState({ todos: response });
                    }
                }
                else {
                    console.error('getTodos failed. Error is: ' + JSON.stringify(error));
                }
            });
    }
    //#endregion.

    //#region Component Life Cycle.
    componentDidMount() {
        this.getTodos();
    }
    //#endregion.

    //#region DOM events.
    //#endregion.

    render() {
        return (
            <div>
                <br />
                <table className="table table-bordered table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th>userId</th>
                            <th>id</th>
                            <th>title</th>
                            <th>completed</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.todos.map((item, i) =>
                            <tr id={item.id} key={item.id}>
                                <td>{item.userId}</td>
                                <td>{item.id}</td>
                                <td>{item.title}</td>
                                <td>{item.completed ? 'true' : 'false'}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}