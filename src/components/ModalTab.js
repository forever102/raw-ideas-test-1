import React from 'react';

// 3rd libs.
import { Button } from 'react-bootstrap';

// Custom Components.
import SampleModal from './SampleModal';

export default class ModalTab extends React.Component {
    constructor(props) {
        super(props);

        // States.

        // Variables.
        this.sampleModalRef = React.createRef();
    }

    //#region Helper methods.
    //#endregion.

    //#region Component Life Cycle.
    componentDidMount() { }
    //#endregion.

    //#region DOM events.
    showSampleModal(e) {
        this.sampleModalRef.current.show();
    }
    //#endregion.
    
    render() {
        return (
            <div>
                <SampleModal ref={this.sampleModalRef} />
                <br />
                <Button href="#" onClick={(e) => this.showSampleModal(e)}>Show Sample Modal</Button>
            </div>
        )
    }
}